using System.Collections.Generic;
using CRM.DTO.General;

namespace CRM.Services.Interface
{
    public interface IListService
    {
        IList<ListViewModel> GetOrganizationTypes();
        IList<ListViewModel> GetCountries();
        List<KeyValuePair<int, string>> GetStatus();
    }
}