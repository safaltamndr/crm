using CRM.DTO.Setup;

namespace CRM.Services.Interface
{
    public interface IOrganizationService
    {
        void AddOrganization(OrganizationRequestViewModel model);
    }
}