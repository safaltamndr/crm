using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using CRM.Data;
using CRM.Domain.Entity.Setup;
using CRM.Domain.Entity.General;
using CRM.DTO.General;
using CRM.General.Enum;
using CRM.Services.Interface;
using System;
using System.Linq;
using Newtonsoft.Json;

namespace CRM.Services
{
    public class ListService : IListService
    {
        private readonly IUnitOfWork<ApplicationContext> _genericUnitOfWork;
        private readonly IMapper _mapper;

        public ListService(IUnitOfWork<ApplicationContext> genericUnitOfWork, IMapper mapper)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }

        public IList<ListViewModel> GetOrganizationTypes()
        {
            var repo = _genericUnitOfWork.GetRepository<OrganizationType>();
            var data = repo.GetList().Items;
            return _mapper.Map<IList<OrganizationType>, IList<ListViewModel>>(data);
        }

        public IList<ListViewModel> GetCountries()
        {
            var repo = _genericUnitOfWork.GetRepository<Country>();
            var data = repo.GetList(x => x.DelFlag == false).Items;
            return _mapper.Map<IList<Country>, IList<ListViewModel>>(data);
        }

        public List<KeyValuePair<int, string>> GetStatus()
        {
            Dictionary<int, string> data = Enum.GetValues(typeof(Status))
                  .Cast<Status>()
                  .ToDictionary(t => (int)t, t => t.ToString());
            var status = data.ToList();
            return status;
        }
    }
}