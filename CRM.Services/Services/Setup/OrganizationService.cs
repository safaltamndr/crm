using System;
using AutoMapper;
using CRM.Common.Configuration;
using CRM.Data;
using CRM.Domain.Entity.Setup;
using CRM.DTO.Setup;
using CRM.Services.Interface;

namespace CRM.Services.General
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IUnitOfWork<ApplicationContext> _genericUnitOfWork;
        private readonly IMapper _mapper;
        public OrganizationService(IUnitOfWork<ApplicationContext> genericUnitOfWork,
IMapper mapper
        )
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }

        public void AddOrganization(OrganizationRequestViewModel model)
        {
            var organizationrepo = _genericUnitOfWork.GetRepository<Organization>();
            var mapped = _mapper.Map<OrganizationRequestViewModel, Organization>(model);
            mapped.Id = Guid.NewGuid();
            organizationrepo.Add(mapped);
            _genericUnitOfWork.SaveChanges();
        }
    }
}