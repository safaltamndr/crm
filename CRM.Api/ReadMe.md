Check Details
----------------------------------------------------------
dotnet new

Create new Application using visual studio core terminal
-----------------------------------------------------------

dotnet new webapi -n SecureAuthentication
dotnet new reactredux -n CRM.WebApp
dotnet new classlib -n CRM.Common

OPEN PROJECT
------------------------------------------------------
code -r SecureAuthentication


TO RUN API
-----------------------------------------------------
dotnet run


TO ADD PACKAGE
---------------------------------------------------------
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer --version 3.1.4
dotnet add package Microsoft.Extensions.Configuration


BUILD PROJECT
-------------------------------------------------------
dotnet build


ADD REFERENCE DLL(CLASS LIBRARY)
---------------------------------------------------------
dotnet add reference C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj 

REMOVE REFERENCE DLL(CLASS LIBRARY)
----------------------------------------------------------
dotnet remove reference C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj

DEPENDENDY INJECTION
-----------------------------------------------------------
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection

  







