TO ADD ENTITY FRAMEWORK PACKAGE 
(STEPS: 1. Nuget.org 2. search for Entity Framework 
	1.a. Microsoft.EntityFrameworkCore 
	1.b Microsoft.EntityFrameworkCore.Design 
	1.c Microsoft.EntityFrameworkCore.SqlServer)
----------------------------------------------------------
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package System.ComponentModel.Annotations

INSTALL dotnet ef
---------------------------------------------------------
dotnet tool install --global dotnet-ef
dotnet tool update --global dotnet-ef


Migration (dotnet ef) doesn't work in Class Library. So need to run migration from WebApp
---------------------------------------------------------------
In multiple context
---------------------
dotnet ef migrations add "Initial Migration" --context "ApplicationContext" --project "C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj" 

In single context
--------------------
dotnet ef migrations add "Initial Migration" --project "C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj" 
    
Remove Migration
--------------------	
dotnet ef migrations remove --project "C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj" 

Update Database
--------------------------
dotnet ef database update

To completely remove all migrations and start all over again, do the following
-------------------------------------------------------------------------------------
dotnet ef database update 0
dotnet ef migrations remove --project "C:\EightSquare\Practise\Projects\crm\CRM.Data\CRM.Data.csproj"   