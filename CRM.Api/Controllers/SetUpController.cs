using System;
using AutoMapper;
using CRM.Domain.Entity.Setup;
using CRM.DTO.Setup;
using CRM.General.Enum;
using CRM.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace CRM.Api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class SetUpController : ControllerBase
    {
        private readonly IOrganizationService _organzationservice;
        private readonly IMapper _mapper;

        public SetUpController(IOrganizationService organzationservice, IMapper mapper)
        {
            _organzationservice = organzationservice;
            _mapper = mapper;
        }
        // public SetUpController()
        // {

        // }

        [HttpPost]
        [Route("organization/add")]
        public IActionResult AddOrganization(OrganizationRequestViewModel model)
        {
            _organzationservice.AddOrganization(model);
            return Ok();
        }

    }
}