// using System;
// using System.Collections.Generic;
// using System.IdentityModel.Tokens.Jwt;
// using System.Linq;
// using System.Security.Claims;
// using System.Text;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Authorization;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.Logging;
// using Microsoft.IdentityModel.Tokens;

// namespace CRM.Api.Controllers
// {
//     [ApiController]
//     [Route("[controller]")]
//     public class AccountController : ControllerBase
//     {
//         private IConfiguration _config;

//         public AccountController(IConfiguration config)
//         {
//             _config = config;
//         }

//         [HttpPost]
//         public IActionResult Login(string UserName, string Password)
//         {
//             IActionResult response = Unauthorized();

//             if (AuthenticateUser(UserName, Password))
//             {
//                 var tokenStr = GenerateJSONWebToken(UserName, Password);
//                 response = Ok(new { token = tokenStr });
//             }
//             return response;
//         }

//         private bool AuthenticateUser(string username, string pwd)
//         {
//             if (username == "testusername" && pwd == "testpwd")
//             {
//                 return true;
//             }
//             return false;
//         }

//         private string GenerateJSONWebToken(string username, string pwd)
//         {
//             var securitykey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
//             var credential = new SigningCredentials(securitykey, SecurityAlgorithms.HmacSha256);

//             var claims = new[]
//             {
//                 new Claim(JwtRegisteredClaimNames.Sub, username),
//                 new Claim(JwtRegisteredClaimNames.Email, pwd),
//                 new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
//             };

//             var token = new JwtSecurityToken(
//                 issuer: _config["Jwt:Issuer"],
//                 audience: _config["Jwt:Issuer"],
//                 claims,
//                  expires: DateTime.Now.AddMinutes(120),
//                  signingCredentials: credential
//             );

//             /*
//             the minimum length in sha256 in 16 characters because they are 256 hexadecimal bits, 
//             then 256/16 = 16
//             */

//             var encodedtoken = new JwtSecurityTokenHandler().WriteToken(token);
//             return encodedtoken;
//         }

//         [Authorize]
//         [HttpPost("Post")]
//         public string Post()
//         {
//             var identity = HttpContext.User.Identity as ClaimsIdentity;
//             IList<Claim> claim = identity.Claims.ToList();
//             var username = claim[0].Value;
//             return "Welcome To : " + username;
//         }

//         [HttpGet("GetValue")]
//         public ActionResult<IEnumerable<string>> Get()
//         {
//             return new string[] { "Value1", "Value2", "Value3" };
//         }
//     }
// }
