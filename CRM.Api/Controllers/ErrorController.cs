using System;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CRM.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class ErrorController : Controller
    {
        [Route("/error-local-development")]
        public IActionResult ErrorLocalDevelopment(
        [FromServices] IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.EnvironmentName != "Development")
            {
                throw new InvalidOperationException(
                    "This shouldn't be invoked in non-development environments.");
            }

            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            return Problem(
                detail: context.Error.StackTrace,
                title: context.Error.Message);
        }

        //[Route("/error")]
        //public IActionResult Error() => Problem();

        [Route("/error")]
        public IActionResult Error() => new ObjectResult(new
        {
            Message = "An error occured while processing your request",
            Code = (int?)HttpStatusCode.InternalServerError
        })
        {
            StatusCode = (int?)HttpStatusCode.InternalServerError
        };
    }

}