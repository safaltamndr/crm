using CRM.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace CRM.Api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class ListController : ControllerBase
    {
        private readonly IListService _listservice;

        public ListController(IListService listservice)
        {
            _listservice = listservice;
        }

        [Route("organization/type")]
        [HttpGet]
        public IActionResult GetTypes()
        {
            return Ok(_listservice.GetOrganizationTypes());
        }

        [Route("country")]
        [HttpGet]
        public IActionResult GetCountries()
        {
            return Ok(_listservice.GetCountries());
        }

        [Route("status")]
        [HttpGet]
        public IActionResult GetStatus()
        {
            return Ok(_listservice.GetStatus());
        }
    }
}