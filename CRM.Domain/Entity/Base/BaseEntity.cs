using System.ComponentModel.DataAnnotations;

namespace CRM.Domain.Entity.Setup.Base
{
    public class BaseEntity<TKey>
    {
        [Required]
        public virtual TKey Id { get; set; }
    }
}