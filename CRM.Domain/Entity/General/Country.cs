using System;
using System.ComponentModel.DataAnnotations;
using CRM.Domain.Entity.Setup.Base;

namespace CRM.Domain.Entity.General
{
    public class Country : BaseEntity<Guid>
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool DelFlag { get; set; }
    }
}