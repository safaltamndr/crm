using System;
using CRM.Domain.Entity.Setup.Base;

namespace CRM.Domain.Entity.Setup
{
    public class OrganizationType : BaseEntity<Guid>
    {
        public string Types { get; set; }
    }
}