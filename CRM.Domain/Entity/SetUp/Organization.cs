
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CRM.Domain.Entity.General;
using CRM.Domain.Entity.Setup.Base;
using CRM.General.Enum;

namespace CRM.Domain.Entity.Setup
{
    public class Organization : BaseEntity<Guid>
    {
        public Organization()
        {
            Id = Guid.NewGuid();
        }
        [StringLength(20)]
        [Required]
        public string Code { get; set; }
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid Type { get; set; }
        [ForeignKey("Type")]
        public virtual OrganizationType OrganizationType{ get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country country { get; set; } 
        [StringLength(100)]
        [Required]
        public string Address { get; set; }
        [StringLength(15)]
        [Required]
        public string Phone1 { get; set; }
        [StringLength(15)]
        public string Phone2 { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(50)]
        public string Url { get; set; }
        [Required]
        public Status IsActive { get; set; }
    }
}