using System;
using CRM.Domain.Entity.Setup;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CRM.Data.Configuration
{
    public class OrganizationTypeConfiguration : IEntityTypeConfiguration<OrganizationType>
    {
        public void Configure(EntityTypeBuilder<OrganizationType> builder)
        {
            builder.HasData(
                new OrganizationType
                {
                    Id = new Guid("0d45eac0-b0a3-471f-b8ad-54a112641703"),
                    Types = "Commercial"
                },
                new OrganizationType
                {
                    Id = new Guid("769afe48-e2c0-4efe-aaaa-d8f371c20d83"),
                    Types = "Development"
                }
            );
        }
    }
}