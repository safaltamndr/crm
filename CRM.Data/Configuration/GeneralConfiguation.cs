using System;
using CRM.Domain.Entity.General;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace  CRM.Data.Configuration
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.HasData(
                new Country
                {
                    Id = new Guid("94736BA5-FD1B-4026-80AE-303BAFC2C87A"),
                    CountryCode = "AU",
                    CountryName="AUSTRALIA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("20F62CE3-7065-4179-A6A9-740812B51462"),
                    CountryCode = "BD",
                    CountryName="BANGLADESH",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("59AF623A-A440-4976-86E6-8E019B0AB387"),
                    CountryCode = "BE",
                    CountryName="BELGIUM",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("0AA7EBC6-00B3-401B-B69D-6A2CAFB771D6"),
                    CountryCode = "KH",
                    CountryName="CAMBODIA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("055CCDD9-DB60-4A56-A436-8B0C5EDE5D37"),
                    CountryCode = "CA",
                    CountryName="CANADA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("3EF59904-860B-471C-98A5-DC59A1FC619A"),
                    CountryCode = "CN",
                    CountryName="CHINA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("CE765A0B-8272-4676-9FB7-7D0A1472FE92"),
                    CountryCode = "HK",
                    CountryName="HONG KONG",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("F49E75C6-69B5-4344-AD1F-91D01E67A5B7"),
                    CountryCode = "IN",
                    CountryName="INDIA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("39F10C53-6396-4D31-BB22-C298ABA0F085"),
                    CountryCode = "ID",
                    CountryName="INDONESIA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("45CBF67B-205D-4423-9C04-3DBC7681CA66"),
                    CountryCode = "JP",
                    CountryName="JAPAN",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("9CA6582C-6243-48CB-8364-E3AE04FBD69A"),
                    CountryCode = "JO",
                    CountryName="JORDAN",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("AB4F8505-ACED-4B7E-A1F0-42F430314128"),
                    CountryCode = "MY",
                    CountryName="MALAYSIA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("546F0090-57B7-447A-B987-EB3D52937D53"),
                    CountryCode = "MU",
                    CountryName="MAURITIUS",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("205F10B4-8F05-4B2E-99D3-E2B238517815"),
                    CountryCode = "MM",
                    CountryName="MYANMAR",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                 new Country
                {
                    Id = new Guid("3F454B2B-64C1-46F3-A715-5BDDAE8244A3"),
                    CountryCode = "NP",
                    CountryName="NEPAL",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("0A54F965-DF64-4610-9444-D6FD66AFE212"),
                    CountryCode = "NZ",
                    CountryName="NEW ZEALAND",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("C0DB27C1-4DEC-4978-92CE-FFF8E57604FB"),
                    CountryCode = "OM",
                    CountryName="OMAN",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("1FAB5891-FCF3-4EC9-B781-04E7146F5F5C"),
                    CountryCode = "PK",
                    CountryName="PAKISTAN",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("3F0DEA3E-C84F-42B6-8D9B-9F16F4C074A7"),
                    CountryCode = "PH",
                    CountryName="PHILIPPINES",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("DEF9E3D8-0902-4FD8-9478-3CB716ED08DA"),
                    CountryCode = "SG",
                    CountryName="SINGAPORE",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("FE6E4FBF-DF90-4FB1-9FD9-1A85ACC9005E"),
                    CountryCode = "ZA",
                    CountryName="SOUTH AFRICA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("423D6DBE-39CD-4013-81E7-847A16454B2B"),
                    CountryCode = "KR",
                    CountryName="SOUTH KOREA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("575BF090-AF0A-413B-B965-C57B4D75B918"),
                    CountryCode = "ES",
                    CountryName="SPAIN",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("5A23C94B-92BB-40F7-988B-E059ABA4C713"),
                    CountryCode = "LK",
                    CountryName="SRI LANKA",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("10430424-E692-48A0-BFEC-A33C2ED23B1D"),
                    CountryCode = "TH",
                    CountryName="THAILAND",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("1319816F-5425-4436-9AA3-B13489D75615"),
                    CountryCode = "AE",
                    CountryName="UNITED ARAB EMIRATES",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("042ED458-F83C-4B86-BF40-BF76C9DDAF47"),
                    CountryCode = "UK",
                    CountryName="UNITED KINGDOM",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("A340CACB-03DF-4E66-9464-DE71C88A6C58"),
                    CountryCode = "US",
                    CountryName="UNITED STATES",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                },
                new Country
                {
                    Id = new Guid("A09588D0-FD44-4ADB-ACCB-06ACC4A4F958"),
                    CountryCode = "VN",
                    CountryName="VIETNAM",
                    CreatedOn = DateTime.Now,
                    DelFlag = false
                }                
            );
        }
    }
}