using Microsoft.EntityFrameworkCore;
using CRM.Data;
using CRM.Domain.Entity.General;
using CRM.Domain.Entity.Setup;
using CRM.Data.Configuration;

namespace CRM.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> opt) : base(opt)
        {

        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationType> OrganizationTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrganizationTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CountryConfiguration());
        }
    }
}