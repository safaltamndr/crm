using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using CRM.Data.Paging;

namespace CRM.Data
{
    public interface IRepositoryReadOnly<T> : IReadRepository<T> where T : class
    {

    }
}