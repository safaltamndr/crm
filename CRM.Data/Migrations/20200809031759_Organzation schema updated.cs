﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRM.Data.Migrations
{
    public partial class Organzationschemaupdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("4f9876b9-ed1a-441c-b90a-3f91a9d685a4"));

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Organizations");

            migrationBuilder.AlterColumn<Guid>(
                name: "Type",
                table: "Organizations",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<Guid>(
                name: "CountryId",
                table: "Organizations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("042ed458-f83c-4b86-bf40-bf76c9ddaf47"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6147));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("055ccdd9-db60-4a56-a436-8b0c5ede5d37"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6092));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0a54f965-df64-4610-9444-d6fd66afe212"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6120));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0aa7ebc6-00b3-401b-b69d-6a2cafb771d6"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6089));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("10430424-e692-48a0-bfec-a33c2ed23b1d"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6142));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1319816f-5425-4436-9aa3-b13489d75615"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6144));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1fab5891-fcf3-4ec9-b781-04e7146f5f5c"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6125));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("205f10b4-8f05-4b2e-99d3-e2b238517815"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6115));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("20f62ce3-7065-4179-a6a9-740812b51462"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6044));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("39f10c53-6396-4d31-bb22-c298aba0f085"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6103));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3ef59904-860b-471c-98a5-dc59a1fc619a"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6095));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f0dea3e-c84f-42b6-8d9b-9f16f4c074a7"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6128));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f454b2b-64c1-46f3-a715-5bddae8244a3"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6118));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("423d6dbe-39cd-4013-81e7-847a16454b2b"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6135));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("45cbf67b-205d-4423-9c04-3dbc7681ca66"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6105));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("546f0090-57b7-447a-b987-eb3d52937d53"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6113));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("575bf090-af0a-413b-b965-c57b4d75b918"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("59af623a-a440-4976-86e6-8e019b0ab387"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6083));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("5a23c94b-92bb-40f7-988b-e059aba4c713"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6139));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("94736ba5-fd1b-4026-80ae-303bafc2c87a"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 105, DateTimeKind.Local).AddTicks(7193));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("9ca6582c-6243-48cb-8364-e3ae04fbd69a"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6108));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a09588d0-fd44-4adb-accb-06acc4a4f958"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6152));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a340cacb-03df-4e66-9464-de71c88a6c58"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6150));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ab4f8505-aced-4b7e-a1f0-42f430314128"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6110));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("c0db27c1-4dec-4978-92ce-fff8e57604fb"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6123));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ce765a0b-8272-4676-9fb7-7d0a1472fe92"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6098));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("def9e3d8-0902-4fd8-9478-3cb716ed08da"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6130));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("f49e75c6-69b5-4344-ad1f-91d01e67a5b7"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6100));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("fe6e4fbf-df90-4fb1-9fd9-1a85acc9005e"),
                column: "CreatedOn",
                value: new DateTime(2020, 8, 9, 9, 2, 59, 106, DateTimeKind.Local).AddTicks(6132));

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_CountryId",
                table: "Organizations",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_Type",
                table: "Organizations",
                column: "Type");

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_Countries_CountryId",
                table: "Organizations",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Organizations_OrganizationTypes_Type",
                table: "Organizations",
                column: "Type",
                principalTable: "OrganizationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_Countries_CountryId",
                table: "Organizations");

            migrationBuilder.DropForeignKey(
                name: "FK_Organizations_OrganizationTypes_Type",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_CountryId",
                table: "Organizations");

            migrationBuilder.DropIndex(
                name: "IX_Organizations_Type",
                table: "Organizations");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Organizations");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Organizations",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<int>(
                name: "Country",
                table: "Organizations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("042ed458-f83c-4b86-bf40-bf76c9ddaf47"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6286));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("055ccdd9-db60-4a56-a436-8b0c5ede5d37"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6147));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0a54f965-df64-4610-9444-d6fd66afe212"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6245));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0aa7ebc6-00b3-401b-b69d-6a2cafb771d6"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6143));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("10430424-e692-48a0-bfec-a33c2ed23b1d"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6279));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1319816f-5425-4436-9aa3-b13489d75615"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6283));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1fab5891-fcf3-4ec9-b781-04e7146f5f5c"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6252));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("205f10b4-8f05-4b2e-99d3-e2b238517815"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6238));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("20f62ce3-7065-4179-a6a9-740812b51462"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6087));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("39f10c53-6396-4d31-bb22-c298aba0f085"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6160));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3ef59904-860b-471c-98a5-dc59a1fc619a"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6151));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f0dea3e-c84f-42b6-8d9b-9f16f4c074a7"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6256));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f454b2b-64c1-46f3-a715-5bddae8244a3"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6242));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("423d6dbe-39cd-4013-81e7-847a16454b2b"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6267));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("45cbf67b-205d-4423-9c04-3dbc7681ca66"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6164));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("546f0090-57b7-447a-b987-eb3d52937d53"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6235));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("575bf090-af0a-413b-b965-c57b4d75b918"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6273));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("59af623a-a440-4976-86e6-8e019b0ab387"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6136));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("5a23c94b-92bb-40f7-988b-e059aba4c713"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6276));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("94736ba5-fd1b-4026-80ae-303bafc2c87a"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 896, DateTimeKind.Local).AddTicks(4734));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("9ca6582c-6243-48cb-8364-e3ae04fbd69a"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6227));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a09588d0-fd44-4adb-accb-06acc4a4f958"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6293));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a340cacb-03df-4e66-9464-de71c88a6c58"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6290));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ab4f8505-aced-4b7e-a1f0-42f430314128"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6232));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("c0db27c1-4dec-4978-92ce-fff8e57604fb"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6248));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ce765a0b-8272-4676-9fb7-7d0a1472fe92"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6154));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("def9e3d8-0902-4fd8-9478-3cb716ed08da"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6260));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("f49e75c6-69b5-4344-ad1f-91d01e67a5b7"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6157));

            migrationBuilder.UpdateData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("fe6e4fbf-df90-4fb1-9fd9-1a85acc9005e"),
                column: "CreatedOn",
                value: new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6264));

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CountryCode", "CountryName", "CreatedOn", "DelFlag", "ModifiedOn" },
                values: new object[] { new Guid("4f9876b9-ed1a-441c-b90a-3f91a9d685a4"), "ZA", "SOUTH AFRICA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6270), false, null });
        }
    }
}
