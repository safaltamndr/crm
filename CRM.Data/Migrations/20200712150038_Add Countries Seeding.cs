﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CRM.Data.Migrations
{
    public partial class AddCountriesSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedOn",
                table: "Countries",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CountryCode", "CountryName", "CreatedOn", "DelFlag", "ModifiedOn" },
                values: new object[,]
                {
                    { new Guid("94736ba5-fd1b-4026-80ae-303bafc2c87a"), "AU", "AUSTRALIA", new DateTime(2020, 7, 12, 20, 45, 37, 896, DateTimeKind.Local).AddTicks(4734), false, null },
                    { new Guid("042ed458-f83c-4b86-bf40-bf76c9ddaf47"), "UK", "UNITED KINGDOM", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6286), false, null },
                    { new Guid("1319816f-5425-4436-9aa3-b13489d75615"), "AE", "UNITED ARAB EMIRATES", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6283), false, null },
                    { new Guid("10430424-e692-48a0-bfec-a33c2ed23b1d"), "TH", "THAILAND", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6279), false, null },
                    { new Guid("5a23c94b-92bb-40f7-988b-e059aba4c713"), "LK", "SRI LANKA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6276), false, null },
                    { new Guid("575bf090-af0a-413b-b965-c57b4d75b918"), "ES", "SPAIN", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6273), false, null },
                    { new Guid("423d6dbe-39cd-4013-81e7-847a16454b2b"), "KR", "SOUTH KOREA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6267), false, null },
                    { new Guid("fe6e4fbf-df90-4fb1-9fd9-1a85acc9005e"), "ZA", "SOUTH AFRICA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6264), false, null },
                    { new Guid("def9e3d8-0902-4fd8-9478-3cb716ed08da"), "SG", "SINGAPORE", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6260), false, null },
                    { new Guid("3f0dea3e-c84f-42b6-8d9b-9f16f4c074a7"), "PH", "PHILIPPINES", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6256), false, null },
                    { new Guid("1fab5891-fcf3-4ec9-b781-04e7146f5f5c"), "PK", "PAKISTAN", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6252), false, null },
                    { new Guid("c0db27c1-4dec-4978-92ce-fff8e57604fb"), "OM", "OMAN", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6248), false, null },
                    { new Guid("0a54f965-df64-4610-9444-d6fd66afe212"), "NZ", "NEW ZEALAND", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6245), false, null },
                    { new Guid("3f454b2b-64c1-46f3-a715-5bddae8244a3"), "NP", "NEPAL", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6242), false, null },
                    { new Guid("205f10b4-8f05-4b2e-99d3-e2b238517815"), "MM", "MYANMAR", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6238), false, null },
                    { new Guid("546f0090-57b7-447a-b987-eb3d52937d53"), "MU", "MAURITIUS", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6235), false, null },
                    { new Guid("ab4f8505-aced-4b7e-a1f0-42f430314128"), "MY", "MALAYSIA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6232), false, null },
                    { new Guid("9ca6582c-6243-48cb-8364-e3ae04fbd69a"), "JO", "JORDAN", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6227), false, null },
                    { new Guid("45cbf67b-205d-4423-9c04-3dbc7681ca66"), "JP", "JAPAN", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6164), false, null },
                    { new Guid("39f10c53-6396-4d31-bb22-c298aba0f085"), "ID", "INDONESIA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6160), false, null },
                    { new Guid("f49e75c6-69b5-4344-ad1f-91d01e67a5b7"), "IN", "INDIA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6157), false, null },
                    { new Guid("ce765a0b-8272-4676-9fb7-7d0a1472fe92"), "HK", "HONG KONG", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6154), false, null },
                    { new Guid("3ef59904-860b-471c-98a5-dc59a1fc619a"), "CN", "CHINA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6151), false, null },
                    { new Guid("055ccdd9-db60-4a56-a436-8b0c5ede5d37"), "CA", "CANADA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6147), false, null },
                    { new Guid("0aa7ebc6-00b3-401b-b69d-6a2cafb771d6"), "KH", "CAMBODIA", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6143), false, null },
                    { new Guid("59af623a-a440-4976-86e6-8e019b0ab387"), "BE", "BELGIUM", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6136), false, null },
                    { new Guid("20f62ce3-7065-4179-a6a9-740812b51462"), "BD", "BANGLADESH", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6087), false, null },
                    { new Guid("a340cacb-03df-4e66-9464-de71c88a6c58"), "US", "UNITED STATES", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6290), false, null },
                    { new Guid("a09588d0-fd44-4adb-accb-06acc4a4f958"), "VN", "VIETNAM", new DateTime(2020, 7, 12, 20, 45, 37, 897, DateTimeKind.Local).AddTicks(6293), false, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("042ed458-f83c-4b86-bf40-bf76c9ddaf47"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("055ccdd9-db60-4a56-a436-8b0c5ede5d37"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0a54f965-df64-4610-9444-d6fd66afe212"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("0aa7ebc6-00b3-401b-b69d-6a2cafb771d6"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("10430424-e692-48a0-bfec-a33c2ed23b1d"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1319816f-5425-4436-9aa3-b13489d75615"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("1fab5891-fcf3-4ec9-b781-04e7146f5f5c"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("205f10b4-8f05-4b2e-99d3-e2b238517815"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("20f62ce3-7065-4179-a6a9-740812b51462"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("39f10c53-6396-4d31-bb22-c298aba0f085"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3ef59904-860b-471c-98a5-dc59a1fc619a"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f0dea3e-c84f-42b6-8d9b-9f16f4c074a7"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("3f454b2b-64c1-46f3-a715-5bddae8244a3"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("423d6dbe-39cd-4013-81e7-847a16454b2b"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("45cbf67b-205d-4423-9c04-3dbc7681ca66"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("4f9876b9-ed1a-441c-b90a-3f91a9d685a4"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("546f0090-57b7-447a-b987-eb3d52937d53"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("575bf090-af0a-413b-b965-c57b4d75b918"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("59af623a-a440-4976-86e6-8e019b0ab387"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("5a23c94b-92bb-40f7-988b-e059aba4c713"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("94736ba5-fd1b-4026-80ae-303bafc2c87a"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("9ca6582c-6243-48cb-8364-e3ae04fbd69a"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a09588d0-fd44-4adb-accb-06acc4a4f958"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("a340cacb-03df-4e66-9464-de71c88a6c58"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ab4f8505-aced-4b7e-a1f0-42f430314128"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("c0db27c1-4dec-4978-92ce-fff8e57604fb"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("ce765a0b-8272-4676-9fb7-7d0a1472fe92"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("def9e3d8-0902-4fd8-9478-3cb716ed08da"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("f49e75c6-69b5-4344-ad1f-91d01e67a5b7"));

            migrationBuilder.DeleteData(
                table: "Countries",
                keyColumn: "Id",
                keyValue: new Guid("fe6e4fbf-df90-4fb1-9fd9-1a85acc9005e"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedOn",
                table: "Countries",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
