using AutoMapper;
using CRM.Domain.Entity.General;
using CRM.DTO.General;

namespace CRM.DTO.Profiles.General
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Country, CountryViewModel>();
        }
    }
}