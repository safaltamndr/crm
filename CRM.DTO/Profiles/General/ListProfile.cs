using AutoMapper;
using CRM.Domain.Entity.Setup;
using CRM.Domain.Entity.General;
using CRM.DTO.General;
using CRM.General.Enum;

namespace CRM.DTO.Profiles.General
{
    public class ListProfile : Profile
    {
        public ListProfile()
        {
            CreateMap<Status, ListViewModel>();
            CreateMap<OrganizationType, ListViewModel>()
                .ForMember(x => x.Text, opts => opts.MapFrom(d => d.Types));
            CreateMap<Country, ListViewModel>()
                .ForMember(x => x.Text, opts => opts.MapFrom(d => d.CountryName));
        }
    }
}