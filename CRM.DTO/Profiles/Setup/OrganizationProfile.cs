using AutoMapper;
using CRM.Domain.Entity.Setup;
using CRM.DTO.Setup;

namespace CRM.DTO.Profiles.Setup
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<Organization, OrganizationRequestViewModel>()
            .ReverseMap();
        }
    }
}