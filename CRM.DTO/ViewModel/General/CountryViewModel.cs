using System;

namespace CRM.DTO.General
{
    public class CountryViewModel
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool DelFlag { get; set; }
    }

}