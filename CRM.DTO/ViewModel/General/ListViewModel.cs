namespace CRM.DTO.General
{
    public class ListViewModel
    {
        public object Id { get; set; }
        public string Text { get; set; }
    }
}