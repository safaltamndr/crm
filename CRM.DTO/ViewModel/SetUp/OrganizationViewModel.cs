using System;
using System.ComponentModel.DataAnnotations;
using CRM.General.Enum;

namespace CRM.DTO.Setup
{
    public class OrganizationRequestViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public Guid Type { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Email { get; set; }

        public string Url { get; set; }
        [Required]
        public Status IsActive { get; set; }
    }

}