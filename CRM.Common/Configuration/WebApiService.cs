﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CRM.Common.Configuration;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace CRM.Common
{
    public class WebApiService
    {
        private static WebApiService _instance;
        public static WebApiService Instance => _instance ?? (_instance = new WebApiService());
        //protected virtual string BaseUri => ConfigurationManager.AppSettings["WebApiUri"];
        public readonly string BaseUri = string.Empty;

        public WebApiService()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();
            BaseUri = root.GetSection("WebApiUri").Value;
        }
        protected virtual string XmlSavePath
        {
            get
            {
                return null;
            }
        }
        public async Task<T> AuthenticateAsync<T>(List<KeyValuePair<string, string>> content, string action = "token")
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = await client.PostAsync(url, new FormUrlEncodedContent(content));

                return ProcessResponse<T>(result, true);
            }
        }
        public T Authenticate<T>(List<KeyValuePair<string, string>> content, string action = "token")
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = client.PostAsync(url, new FormUrlEncodedContent(content)).Result;

                return ProcessResponse<T>(result, true);
            }
        }
        public async Task<T> GetAsync<T>(string action)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = await client.GetAsync(url);
                return ProcessResponse<T>(result);
            }
        }
        public T Get<T>(string action)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = client.GetAsync(url).Result;
                return ProcessResponse<T>(result);
            }
        }
        public void Get(string action)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = client.GetAsync(url).Result;
                ProcessResponse(result);
            }
        }
        public async Task GetAsync(string action)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action);

                var result = await client.GetAsync(url);
                ProcessResponse(result);
            }
        }
        public async Task<T> PostAsync<T>(string action, object data)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action, data);

                var result = await client.PostAsJsonAsync(url, data);
                return ProcessResponse<T>(result);
            }
        }
        public async Task PostAsync(string action, object data = null)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action, data);
                var result = await client.PostAsJsonAsync(url, data);
                ProcessResponse(result);
            }
        }
        public void Post(string action, object data = null)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action, data);

                var result = client.PostAsJsonAsync(url, data).Result;
                ProcessResponse(result);
            }
        }
        public T Post<T>(string action, object data)
        {
            using (var client = new HttpClient())
            {
                var url = GetUri(client, action, data);
                var result = client.PostAsJsonAsync(url, data).Result;
                return ProcessResponse<T>(result);
            }
        }
        private T ProcessResponse<T>(HttpResponseMessage response, bool replaceSpecialCharacters = false)
        {
            var action = response.RequestMessage.RequestUri.AbsoluteUri;
            var json = response.Content.ReadAsStringAsync().Result;
            if (response.IsSuccessStatusCode)
            {
                var res = replaceSpecialCharacters ?
                    json.Replace("\\\"", "\"")
                        .Replace("\"[", "[")
                        .Replace("]\"", "]")
                        .Replace("\"{", "{")
                        .Replace("}\"", "}") : json;

                T result;
                res = res.Trim();
                var mediaType = response.Content.Headers.ContentType.MediaType;
                if ((mediaType == "text/html" && res.StartsWith("<")) || mediaType.Contains("xml"))
                    result = XmlHelper.Deserialize<T>(res);
                else
                    result = JsonConvert.DeserializeObject<T>(res);

                SaveXml(action, result);
                return result;
            }
            throw new HttpResponseException
            {
                Status = (int)response.StatusCode,
                Value = json,
                Code = "0002"
            };
        }
        private void ProcessResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }
            var json = response.Content.ReadAsStringAsync().Result;
            throw new HttpResponseException
            {
                Status = (int)response.StatusCode,
                Value = json,
                Code = "0002"
            };
        }
        protected virtual string GetUri(HttpClient client, string action, object data = null)
        {
            //client.DefaultRequestHeaders.AddHeaders();
            return BaseUri.TrimEnd('/') + '/' + action.TrimStart('/');
        }
        protected virtual void SaveXml(string action, object data)
        {
            // if (XmlSavePath != null)
            //     new ApiXml { Url = action, Data = data }.SaveApiXml($"{GetFileName(action)}_Response" + DateTime.Now.ToString("yyyyMMddHHmmss"), XmlSavePath);
        }
        protected void SaveRequest(string action, object data)
        {
            // if (XmlSavePath != null)
            //     new ApiXml { Url = action, Data = data }.SaveApiXml($"{GetFileName(action)}_Request" + DateTime.Now.ToString("yyyyMMddHHmmss"), XmlSavePath);
        }
        private string GetFileName(string action)
        {
            var name = action.Replace(BaseUri, "").Replace("/", "");
            return name.First().ToString().ToUpper() + name.Substring(1);
        }
    }
}