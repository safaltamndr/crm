﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CRM.Common
{
    public static class XmlHelper
    {
        public static DataSet ReadFromXml(string filePath)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(filePath, XmlReadMode.InferSchema);
            return ds;
        }
        public static string XmlToString(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            string xmlcontents = doc.InnerXml;
            return xmlcontents;
        }
        public static string SaveXml(object obj, string fileName, string xmlPath, List<Type> types = null)
        {

            string xml = string.Empty;
            xml = SerializeObject(obj, obj.GetType(), types);
            try
            {
                var xmlDoc = new XmlDocument();
                xml = xml.Substring(39);
                xmlDoc.LoadXml(xml);
                if (!Directory.Exists(xmlPath))
                    Directory.CreateDirectory(xmlPath);
                var strFullPath = xmlPath + fileName + ".xml";
                xmlDoc.Save(strFullPath);
            }
            catch
            {
                //don't do anything 
            }

            return xml;
        }
        public static T GetSavedXmlObject<T>(string fileName, string xmlPath)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(fileName)) return default(T);

            try
            {
                //string xmlPath = Path.Combine(@"" + ConfigurationManager.AppSettings["ioxmlpath"], @"IOXML\");
                var strFullPath = xmlPath + fileName + ".xml";
                StreamReader xmlStream = new StreamReader(strFullPath);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch
            {
                // ignored
            }
            return returnObject;

        }
        private static string SerializeObject(Object pObject, Type typClassName, List<Type> types = null)
        {
            String xmlizedString = null;
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typClassName);
                if (types != null)
                {
                    xs = new XmlSerializer(typClassName, types.ToArray());
                }
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xs.Serialize(xmlTextWriter, pObject);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                xmlizedString = Utf8ByteArrayToString(memoryStream.ToArray());
            }
            catch
            {
                //do nothing 
            }
            return xmlizedString;
        }
        public static T Deserialize<T>(string xmlObject)
        {
            T returnObject = default(T);
            try
            {
                TextReader xmlString = new StringReader(xmlObject);
                XmlSerializer xs = new XmlSerializer(typeof(T));
                returnObject = (T)xs.Deserialize(xmlString);
            }
            catch
            {

            }
            return returnObject;

        }
        public static Object XMLToObject(string xml, Type objectType)
        {
            StringReader strReader = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            Object obj = null;
            try
            {

                strReader = new StringReader(xml);
                serializer = new XmlSerializer(objectType);
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            return obj;
        }
        private static String Utf8ByteArrayToString(Byte[] characters)
        {
            String constructedString = "";
            try
            {
                UTF8Encoding encoding = new UTF8Encoding();
                constructedString = encoding.GetString(characters);
            }
            catch
            {
                //do nothing 
            }
            return (constructedString);
        }
    }
}