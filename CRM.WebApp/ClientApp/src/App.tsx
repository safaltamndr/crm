import * as React from "react";
import { Route } from "react-router";
import Layout from "./components/Layout";
import Home from "./components/Home";
import Counter from "./components/Counter";
import FetchData from "./components/FetchData";
import Organization from "./components/Setup/organization";
import Basic from "./components/Research/orgform";
import Reacthookformcheck from "./components/Setup/reacthookform";

import "./custom.css";

export default () => (
  <Layout>
    <Route exact path="/" component={Home} />
    <Route path="/counter" component={Counter} />
    <Route path="/organization" component={Organization} />
    <Route path="/fetch-data/:startDateIndex?" component={FetchData} />
    <Route path="/orgform" component={Basic} />
    <Route path="/reacthookform" component={Reacthookformcheck} />
  </Layout>
);
