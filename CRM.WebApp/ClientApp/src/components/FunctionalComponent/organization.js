import React from "react";
import { useForm } from "react-hook-form";
import { DropDownList } from "@progress/kendo-react-dropdowns";

const Basic = (props) => {
  const { handleSubmit, register, errors } = useForm();
  const { details, countries, status, handleSubmitOrganization } = props;
  //const onSubmit = (values) => handleSubmitOrganization(values);

  return (
    <form onSubmit={handleSubmit(handleSubmitOrganization)}>
      <div className="row">
        <div className="col-md-4">
          <div className="form-group">
            <label>Code</label>
            <input
              className="form-control"
              type="text"
              id="code"
              name="code"
              style={{borderColor: errors.code && "red"}}
              ref={register({
                required: "Required",
                minLength: 6,
                maxLength: 20,
                pattern: /^[A-Za-z0-9]+$/i,
              })}
            ></input>    
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label>Name</label>
            <input
              className="form-control"
              type="text"
              id="name"
              name="name"
              style={{borderColor: errors.name && "red"}}
              ref={register({
                required: "Required",
                minLength: 6,
                maxLength: 20,
                pattern: /^[A-Za-z0-9]+$/i,
              })}
            ></input>
          </div>
        </div>
        <div className="col-md-4">
              <div className="form-group">
                <label>Type</label>
                <DropDownList
                  name ="type"
                  className="form-control"
                  data={details}
                  textField="text"
                  dataItemKey="id"
                  style={{borderColor: errors.type && "red"}}
                  ref={register({
                    required: "Required"
                  })}
                />
              </div>
         </div>
      </div>
      <div className="row">
        <div className="col-md-4">
          <div className="form-group">
            <label>Country</label>
            <DropDownList
                  name ="country"
                  className="form-control"
                  data={countries}
                  textField="text"
                  dataItemKey="id"
                  style={{borderColor: errors.country && "red"}}
                  ref={register({
                    required: "Required"
                  })}
                />
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label>Address</label>
            <input
              className="form-control"
              type="text"
              id="address"
              name="address"
              style={{borderColor: errors.address && "red"}}
              ref={register({
                required: "Required",
                minLength: 6,
                maxLength: 20
              })}
            ></input>
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label>Phone 1</label>
            <input
              className="form-control"
              type="text"
              id="phone1"
              name="phone1"
              style={{borderColor: errors.phone1 && "red"}}
              ref={register({
                required: "Required",
                minLength: 6,
                maxLength: 20
              })}
            ></input>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-4">
          <div className="form-group">
            <label>Phone 2</label>
            <input
              className="form-control"
              type="text"
              id="phone2"
              name="phone2"
              ref={register()}
            ></input>
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label>Email</label>
            <input
              className="form-control"
              type="text"
              id="email"
              name="email"
              style={{borderColor: errors.email && "red" }}
              ref={register({
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "invalid email address"
                }
              })}
            ></input>
            <div style={{color:"red"}}> 
            {errors.email && errors.email.message}
            </div> 
          </div>
        </div>
        <div className="col-md-4">
          <div className="form-group">
            <label>Url</label>
            <input
              className="form-control"
              type="text"
              id="url"
              name="url"
              ref={register()}
            ></input>
          </div>
        </div>        
      </div>
      <div className="row">
      <div className="col-md-4">
          <div className="form-group">
            <label>Status</label>
            <DropDownList
                  name = "status"
                  className="form-control"
                  data={status}
                  textField="Value"
                  dataItemKey="Key"
                  ref={register}
                  value={{ Value: 'Active', Key: 1}}
                />
          </div>
        </div>
        </div>
      {/* <button className="primary">Save</button> */}
      <button type="submit">Submit</button>
    </form>
  );
};
export default Basic;
