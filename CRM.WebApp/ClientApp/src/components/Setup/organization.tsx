import React, { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Basic from "../FunctionalComponent/organization";

class Reacthookformcheck extends Component {
  constructor(props: any) {
    super(props);
    this.handleSubmitOrganization= this.handleSubmitOrganization.bind(this);
  }
  state = {
    countries: [],
    types: [],
    status: []
  };
   
  componentDidMount() {    
    fetch("https://localhost:5001/api/list/organization/type")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let typelist = data;
      this.setState({
        types: typelist,
      });
    })
    .catch((error) => {
      console.log(error);
    });

     /* Fetch Countries */
     fetch("https://localhost:5001/api/list/country")
     .then((response) => {
       return response.json();
     })
     .then((data) => {
       let countrylist = data;
       this.setState({
         countries: countrylist,
       });
     })
     .catch((error) => {
       console.log(error);
     });
     
    /* Fetch status */
    fetch("https://localhost:5001/api/list/status")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let status = data;
      this.setState({
        status: status,
      });
    })
    .catch((error) => {
      console.log(error);
    });
  }
  

  handleSubmitOrganization = (organizationddetails : any)=> {
   console.log(JSON.stringify({
    Id: "00000000-0000-0000-0000-00000000000",
    Code: organizationddetails.code,
    Name: organizationddetails.name,
    Type: organizationddetails.type.id,
    CountryId: organizationddetails.country.id,
    Address: organizationddetails.address,
    Phone1: organizationddetails.phone1,
    Phone2: organizationddetails.phone2,
    Email: organizationddetails.email,
    Url: organizationddetails.url,
    IsActive: organizationddetails.status.Key,
    }));
    fetch("https://localhost:5001/api/setup/organization/add", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Id: "00000000-0000-0000-0000-000000000000",
        Code: organizationddetails.code,
        Name: organizationddetails.name,
        Type: organizationddetails.type.id,
        CountryId: organizationddetails.country.id,
        Address: organizationddetails.address,
        Phone1: organizationddetails.phone1,
        Phone2: organizationddetails.phone2,
        Email: organizationddetails.email,
        Url: organizationddetails.url,
        IsActive: organizationddetails.status.Key,
      }),
    })
      .then(function (response) {
        // Success 
        return response.json();
      })
      .then(function (data) {
        if(data.code == "0002")
          toast.error(data.message);
        else
          toast.success("Successfully Submitted");
      })
      .catch((error) => {
        toast.error(error.message);
      });;
   
  }

  render() {
    return (
      <div>
        <Basic
           details={this.state.types} 
           countries={this.state.countries}
           status ={this.state.status}
           handleSubmitOrganization={this.handleSubmitOrganization}
           >             
           </Basic>
           <ToastContainer />
      </div>
    );
  }
}

export default Reacthookformcheck;
