import React, { Component } from "react";
import Basic from "../FunctionalComponent/organization";

class Reacthookformcheck extends Component {
  state = {
    orgname: "ORGNAME",
  };
  render() {
    return (
      <div>
        <Basic details={this.state.orgname}></Basic>
      </div>
    );
  }
}

export default Reacthookformcheck;
