>> https://babeljs.io/
convert JSX ( javascript xml to plain javascript code)

Install Extensions from Visual Studio Code

>> Simple React Snippets
>> prettier - code formatter

Bootstrap in terminal 
----------------------------------
>>npm i bootstrap@4.1.1

Install react-hook-form
----------------------------------
npm install --save react-hook-form
version: 3.26.3-beta.1 (worked with React:16.0.1)

Install npm
----------------------------------
npm install --save react@latest

Install latest typescript
---------------------------------
npm install --save typescript@latest 


Install DropDown
----------------------------------
>> npm install --save @progress/kendo-react-dropdowns @progress/kendo-react-intl
>> npm install --save @progress/kendo-theme-default

Install Toastr
----------------------------------
npm install --save react-toastify

Certificate Issue
--------------------------------------
dotnet dev-certs https --clean
dotnet dev-certs https --trust

----------------------------------
Short cuts
cntrl + P (search)
cntrl + d (to change all text at once)