using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using CRM.Common;
using CRM.Domain.Entity.Setup;
using CRM.DTO.General;
using Microsoft.AspNetCore.Mvc;

namespace CRM.WebApp.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class ListController : ControllerBase
    {
        public ListController()
        {

        }

        [Route("organization/type")]
        [HttpGet]
        public async Task<IActionResult> GetTypes()
        {
            return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/organization/type"));
        }

        [Route("country")]
        [HttpGet]
        public async Task<IActionResult> GetCountries()
        {
            return Ok(await WebApiService.Instance.GetAsync<IList<ListViewModel>>("list/country"));
        }

        [Route("status")]
        [HttpGet]
        public async Task<IActionResult> GetStatus()
        {
            return Ok(await WebApiService.Instance.GetAsync<List<KeyValuePair<int, string>>>("list/status"));
        }
    }
}