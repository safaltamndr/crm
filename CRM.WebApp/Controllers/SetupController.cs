using System.Threading.Tasks;
using CRM.Common;
using CRM.DTO.Setup;
using Microsoft.AspNetCore.Mvc;

namespace CRM.WebApp.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class SetupController : ControllerBase
    {
        public SetupController()
        {

        }

        [Route("organization/add")]
        [HttpPost]
        public async Task<IActionResult> AddOrganization(OrganizationRequestViewModel model)
        {
            await WebApiService.Instance.PostAsync("setup/organization/add", model);
            return Ok();
        }

    }
}