using System;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using CRM.Common.Configuration;

namespace CRM.WebApp.Controllers
{
    [Route("api")]
    [ApiController]
    public class ErrorController : Controller
    {
        [Route("/error-local-development")]
        public IActionResult ErrorLocalDevelopment(
        [FromServices] IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.EnvironmentName != "Development")
            {
                throw new InvalidOperationException(
                    "This shouldn't be invoked in non-development environments.");
            }

            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
     
           throw new HttpResponseException
            {
                Status = 500,
                Value = context.Error.Message,
                Code = "0002"
            };
        }

        [Route("/error")]
        public IActionResult Error() => new ObjectResult(new
        {
            Message = "An error occured while processing your request",
            Code = (int?)HttpStatusCode.InternalServerError
        })
        {
            StatusCode = (int?)HttpStatusCode.InternalServerError
        };
    }

}